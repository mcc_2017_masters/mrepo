import unittest
import BinarySearch.py

class TestBinarySearch(unittest.TestCase):
  def test_smoke(self):
    self.assertEqual(binarySearch([1],1), "TRUE")
    self.assertEqual(binarySearch([1],2), "FALSE")
    self.assertEqual(binarySearch([1,2,3],1), "TRUE")
    self.assertEqual(binarySearch([1,2,3],2), "TRUE")
    self.assertEqual(binarySearch([1,2,3],3), "TRUE")
    self.assertEqual(binarySearch([1,2,3],4), "FALSE")
    
  def test_happy(self):
    self.assertEqual(binarySearch([0],0), "TRUE")
    self.assertEqual(binarySearch([-1],-1), "TRUE")
    self.assertEqual(binarySearch([0],1), "FALSE")
    self.assertEqual(binarySearch([0],-1), "FALSE")
    self.assertEqual(binarySearch([-1],1), "FALSE")
    self.assertEqual(binarySearch([-1],0), "FALSE")
    
  def test_boundary(self):
    self.assertEqual(binarySearch([sys.maxint],maxint), "TRUE")
    self.assertEqual(binarySearch([(-sys.maxint -1)],(-maxint -1)), "TRUE")
    self.assertEqual(binarySearch([sys.maxint],(maxint-1)), "FALSE")
    self.assertEqual(binarySearch([(-sys.maxint -1)],maxint), "FALSE")
    self.assertEqual(binarySearch([sys.maxint],(-maxint -1)), "FALSE")
    
