import random


class CowsAndBulls:
    @property
    def genRandom(self):
        return self.__genRandom

    @property
    def userGuesses(self):
        return self.__userGuesses

    def __init__(self):
        # Initialize current random number
        self.__genRandom = self.getrandomnumber()

        # Initialize user guesses
        self.__userGuesses = 0

    def getrandomnumber(self):
        rdm = str(int(random.random() * 9) + 1)
        while len(rdm) < 4:
            rdmTmp = str(int(random.random() * 9))
            if rdmTmp in rdm:
                continue
            else:
                rdm += rdmTmp
        return int(rdm)

    def cowsAndBulls(self, guess):
        bulls = 0
        cows = 0

        # Increment number of tries
        self.__userGuesses += 1

        # Validate guess is an int or if its length is different than 4
        if not isinstance(guess, int) or len(str(guess)) != 4:
            print "Not a valid integer: " + str(guess)
            return False

        # Validate guess does not have repeated numbers
        for idx in range(len(str(guess))):
            for j in range(idx + 1, len(str(guess)), 1):
                if str(guess)[idx] == str(guess)[j]:
                    print "Not a valid integer: " + str(guess) + ". There are numbers repeated in the given number."
                    return False

        # Review if guess and genRandom are equal
        if guess == self.__genRandom:
            print "4 cows, 0 bulls. Tries %d. You Won!!" % self.userGuesses
            return True

        guessStr = str(guess)
        genRdmStr = str(self.__genRandom)

        for idx in range(len(guessStr)):
            if guessStr[idx] == genRdmStr[idx]:
                cows += 1
            elif guessStr[idx] in genRdmStr:
                bulls += 1

        if cows != 1 and bulls != 1:
            print "%d cows, %d bulls." % (cows, bulls)
            return False
        if cows == 1 and bulls != 1:
            print "%d cow, %d bulls." % (cows, bulls)
            return False
        if cows != 1 and bulls == 1:
            print "%d cows, %d bull." % (cows, bulls)
            return False
        else:
            print "%d cow, %d bull." % (cows, bulls)
            return False

    def start(self):
        userGuess = ""
        gameFinish = False
        while not gameFinish:
            while True:
                try:
                    userGuess = int(raw_input("Please provide a four digits integer: "))
                    if len(str(userGuess)) != 4:
                        print "Invalid integer number"
                    else:
                        # Validate guess does not have repeated numbers
                        for idx in range(len(str(userGuess))):
                            for j in range(idx + 1, len(str(userGuess)), 1):
                                if str(userGuess)[idx] == str(userGuess)[j]:
                                    raise ValueError()

                        # Number accepted
                        break
                except ValueError:
                    print "Invalid number"
            print "***********************************"
            gameFinish = self.cowsAndBulls(userGuess)


cab = CowsAndBulls()
cab.start()
