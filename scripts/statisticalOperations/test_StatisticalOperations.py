import unittest
from StatisticalOperations import Statical


class TestUM(unittest.TestCase):
    def setUp(self):
        pass

    def test_mean(self):
        sts = Statical()
        self.assertEqual(sts.get_mean(), 49)

    def test_std_dvt(self):
        sts = Statical()
        self.assertEqual(sts.get_standard_deviation(), 28.965496715920477)

    def test_mode(self):
        sts = Statical()
        self.assertEqual(sts.get_mode(), 33)

    def test_quartiles(self):
        sts = Statical()
        self.assertEqual(sts.get_quartiles(), [25, 51, 75])

    def test_linear_regression(self):
        sts = Statical()
        str = 'linear regresion Parameters: a: ', 196, ' b: ', -3
        self.assertEqual(sts.get_linear_regresion(), str)


if __name__ == '__main__':
    unittest.main()

# def main():
#     sts = Statical()
#     print "Mean: "
#     print sts.get_mean()
#     print "Standard deviation: "
#     print sts.get_standard_deviation()
#     print "Mode: "
#     print sts.get_mode()
#     print "Quartiles: "
#     print sts.get_quartiles()
#     sts.get_linear_regresion()