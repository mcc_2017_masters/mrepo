import unittest
from ListDuplicates import removeDuplicates
from ListDuplicates import removeDuplicatesLoop


class TestUM(unittest.TestCase):
    def setUp(self):
        pass

    #removeDuplicates
    def test_RemDup1(self):
        self.assertEqual(removeDuplicates([213,123,12,13,123]), [123, 12, 213, 13])

    #removeDuplicatesLoop
    def test_RemDupLoo1(self):
        self.assertEqual(removeDuplicatesLoop([213,123,12,13,123]), [213, 12, 13, 123])

    #removeDuplicates
    def test_RemDup2(self):
        self.assertEqual(removeDuplicates([1,2,3,1,2,3,4,5,2,6,9,7,8,8]), [1, 2, 3, 4, 5, 6, 7, 8, 9])

    #removeDuplicatesLoop
    def test_RemDupLoo2(self):
        self.assertEqual(removeDuplicatesLoop([1,2,3,1,2,3,4,5,2,6,9,7,8,8]), [1, 3, 4, 5, 2, 6, 9, 7, 8])

    #removeDuplicates
    def test_RemDup3(self):
        self.assertEqual(removeDuplicates([]), [])

    #removeDuplicatesLoop
    def test_RemDupLoo3(self):
        self.assertEqual(removeDuplicatesLoop([]), [])



if __name__ == '__main__':
    unittest.main()