def isList(listToVal):
    if isinstance(listToVal, list):
        return True
    else:
        return False


def removeDuplicates(origList):
    if not isList(origList):
        print "Not a list, please introduce a list!"
        return
    return list(set(origList))


def removeDuplicatesLoop(origList):
    if not isList(origList):
        print "Not a list, please introduce a list!"
        return
    newList = []
    for idx in range(len(origList)):
        found = False
        for j in range(idx+1, len(origList), 1):
            if origList[idx] == origList[j]:
                found = True
                break
        if not found:
            newList.append(origList[idx])
    return newList

# Test 1
#print removeDuplicates([1,2,3,1,2,3,4,5,2,6,9,7,8,8])
#print removeDuplicatesLoop([213,123,12,13,123])


# Test 2
print removeDuplicates([])
print removeDuplicatesLoop([])

# Test 3
#print removeDuplicates("Hello!")
#print removeDuplicatesLoop("Hello!")
