# tests.py
import unittest
from ddt import ddt, data
from concatenate_2_strings import randomwords

@ddt
class testing_2_strings(unittest.TestCase):

    for i in range(1000):
        @data(i)
        def test(self, value):
            self.assertFalse(randomwords(value))
