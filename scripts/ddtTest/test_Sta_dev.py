import unittest
from ddt import ddt, data
from Standar_dev import get_standard_deviation

@ddt
class testing_file(unittest.TestCase):

    for i in range(20000):
        @data(i)
        def test_file_stadev(self, value):
            print value
            self.assertTrue(get_standard_deviation(value))



