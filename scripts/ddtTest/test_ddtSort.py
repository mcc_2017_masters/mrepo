import unittest
from ddt import ddt, data

def isSortList(lst):
  return (lst == sorted(lst))

@ddt
class FooTestCase(unittest.TestCase):
  for i in range(4000):
    @data(i)
    def test(self, value):
      mylist = [value]
      self.assertTrue(isSortList(mylist))
        
