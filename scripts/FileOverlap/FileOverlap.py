def overlap_numbers():
    happyNumbers = open("happynumbers.txt", "r")
    primeNumbers = open("primenumbers.txt", "r")
    output = open("output.txt", "w")
    
    happyNumbersList = []
    primeNumbersList = []
    
    for line in happyNumbers:
        happyNumbersList.append(int(line))
    
    for line in primeNumbers:
        primeNumbersList.append(int(line))
    
    for num in list(set(happyNumbersList) & set(primeNumbersList)):
        output.write(str(num) + "\n")
    
    happyNumbers.close()
    primeNumbers.close()
    output.close()