def binarysearch(myarray, target):
    """ binarysearch
        Given an integer array: myarray, search for value target.
        Returns "TRUE" if value found.
        Returns "FALSE" otherwise.
    """
    indstart = 0
    indend = len(myarray)
    while indstart < indend:
        mid = (indstart+indend)//2
        midval = myarray[mid]
        if midval < target:
            indstart = mid+1
        elif midval > target:
            indend = mid
        else:
            return "TRUE"
    return "FALSE"


#Testcases
#result = "FAIL"
#testarray = [1,2,3,5,8]
#findnumber = 6
#result = binarysearch([1,2,3,5,8], 6)
#print "Test 1 result %s" % result
#result = binarysearch([1,2,3,5,8], 5)
#print "Test 2 result %s" % result
#testarray = [1,2,3,5,8,10,11,15]
#print " == array under test %s ==" % testarray
#for findnumber in range(10):
#    result = binarysearch(testarray, findnumber)
#    print "Test iteration %d result %s" % (findnumber, result)