import unittest
from BinarySearch import binarysearch


class testBinarySearch(unittest.TestCase):
    def setUp(self):
        pass

    def testSmoke(self):
        self.assertIsInstance(binarysearch([1],1), "TRUE")
        self.assertIsInstance(binarysearch([1],2), "FALSE")
        
    def testThree(self):
        self.assertIsInstance(binarysearch([0,1,2],0), "TRUE")
        self.assertIsInstance(binarysearch([0,1,2],1), "TRUE")
        self.assertIsInstance(binarysearch([0,1,2],2), "TRUE")
        self.assertIsInstance(binarysearch([0,1,2],4), "FALSE")
        
    def testNegativeInteger(self):
        self.assertIsInstance(binarysearch([0,-2,-3],0), "TRUE")
        self.assertIsInstance(binarysearch([0,-1,-2],-1), "TRUE")
        self.assertIsInstance(binarysearch([0,-1,-2],-2), "TRUE")
        self.assertIsInstance(binarysearch([0,-1,-2],4), "FALSE")
        self.assertIsInstance(binarysearch([0,-1,-2],-4), "FALSE")
    
if __name__ == '__main__':
    unittest.main()