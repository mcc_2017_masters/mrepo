def assignValues():
    global a
    global b
    a = 3
    b = 7
    return a,b


class OddOrEven:

    def __init__(self, num, check):
        import operator
        if not num and operator.ne(num, 0):
            print 'Invalid Integer Number'
            return
        else:
            self.num = num

        if not check:
            if operator.eq(check, 0):
                print "Division by 0 is not valid"
                return
        else:
            self.check = check

    # If number is divisible by 4 print is even and multiple of four. If is divisible by 2 print is even, else is odd.
    def isFourMultipleOrEvenOrOdd(self):
        import operator
        if operator.eq(operator.mod(self.num, 4), 0):
            print "Number " + str(self.num) + " is even and also is multiple of four!"
        elif operator.eq(operator.mod(self.num, 2), 0):
            print "Number " + str(self.num) + " is even!"
        else:
            print "Number " + str(self.num) + " is odd!"

        return 1

    # Test if num is divisible of check
    def isNumDivisibleOfCheck(self):
        import operator
        if operator.eq(operator.mod(self.num, self.check), 0):
            print "Number " + str(self.num) + " is divisible by " + str(self.check) + "!"
        else:
            print "Number " + str(self.num) + " not divisible by " + str(self.check) + "!"

        return 1

    # Check if num is prime
    def isNumPrime(self):
        import operator
        if operator.eq(self.num, 2):
            print "Number 2 is prime!"
            return self.num
        elif operator.eq(operator.mod(self.num, 2), 0):
            print "Number " + str(self.num) + " is not prime!"
            return self.num

        idx = 3
        while not operator.gt(idx, self.num):
            if operator.eq(idx, self.num):
                print "Number " + str(self.num) + " is prime!"
                break
            j = 1
            while not operator.gt(operator.mul(idx, j), self.num):
                if operator.eq(operator.mul(idx, j), self.num):
                    print "Number " + str(self.num) + " is not prime!"
                    return
                j += 1
            idx += 2





# Test cases
print("Test case 1")
c=assignValues()
oddOrEven1 = OddOrEven(a, b)
oddOrEven1.isFourMultipleOrEvenOrOdd()
oddOrEven1.isNumDivisibleOfCheck()
oddOrEven1.isNumPrime()



