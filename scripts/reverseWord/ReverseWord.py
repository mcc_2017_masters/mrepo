def reverseWord(word):
    if not isinstance(word, basestring):
        return "Word: \'" + str(word) + "\' is not an string!"

    wList = word.split(" ")
    myWord = ""

    for i in range(len(wList)):
        myWord = myWord + wList[len(wList)-i-1] + " "
    return myWord.strip()

# Test cases
# print(reverseWord("Hello, how are you?"))
# print(reverseWord("My name is Michele"))
# print(reverseWord(""))
# print(reverseWord(1))