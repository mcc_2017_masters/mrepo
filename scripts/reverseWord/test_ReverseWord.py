import unittest
from ReverseWord import reverseWord


class TestUM(unittest.TestCase):
    def setUp(self):
        pass

    def test_String(self):
        self.assertEqual(reverseWord("Hello, how are you?"), "you? are how Hello,")

    def test_string_2(self):
        self.assertEqual(reverseWord("My name is Michele"), 'Michele is name My')

    def test_empty_string(self):
        self.assertEqual(reverseWord(""), '')

    def test_num(self):
        self.assertEqual(reverseWord(1), "Word: '1' is not an string!")


if __name__ == '__main__':
    unittest.main()