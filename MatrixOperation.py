#MATRIX
#!/usr/bin/python
from random import randint
import unittest  # This loads the testing methods and a main program
"""This loads the testing methods and a main program """

""" UnitTesting """

class TestUM(unittest.TestCase):
    def setUp(self):
        pass


"""  Populates arrays randomly to calculate determinant number  """
def determinante():

    """  1x1 determinant """ 
    M1 = [0]
    M1[0]= randint(0, 5)
    det = M1[0]
    print 'Matriz 1x1 = ', M1
    print 'El determinante es: ', det

    """  2x2 determinant """ 
    n=2
    m=2
    M2 = [[0,0],[0,0]]
    for i in range(n):
        for j in range(m):
            M2[i][j] = randint(0, 5)

    det = (M2[0][0] * M2[1][1]) - (M2[1][0] * M2[0][1])
    print 'Matriz 2x2 = ', M2
    print 'El determinante es: ', det

    """   3x3 determinant """  
    n=3
    m=3
    M3 = [[0,0,0],[0,0,0],[0,0,0]]
    for i in range(n):
        for j in range(m):
            M3[i][j]=randint(0,5)

    det = M3[0][0]*(M3[1][1]*M3[2][2] - M3[1][2]*M3[2][1]) - M3[0][1]*(M3[1][0]*M3[2][2] - M3[1][2]*M3[2][0]) + M3[0][2]*(M3[1][0]*M3[2][1] - M3[1][1]*M3[2][0])
    print 'Matriz 3x3 = ', M3
    print 'El determinante es: ', det

    return det

"""  main, used to kick off procedure and unittesting """  
if __name__ == '__main__':
    determinante()
    unittest.main()